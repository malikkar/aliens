#include <stdio.h>
#include <math.h>

int main() {
    char message1[1000];
    char message2[1000];
    int pulse_length1 = 0;
    int pulse_length2 = 0;
    int message_length1 = 0;
    int message_length2 = 0;
    int palka = 0;
    int j = 0;

    printf("Zpravy:\n");

    scanf("%s",message1);
    scanf("%s",message2);

    if ((message1[0] == '|' && message1[1] == '\0') || (message2[0] == '|' && message2[1] == '\0')) {
        printf("Nespravny vstup.\n");
        return 1;
    }

    // Cteni pulzu prvni zpravy
    while (message1[palka] != '|')
    {
        if (message1[palka] >= 'a' && message1[palka] <= 'z' && message1[palka] != EOF)
        {
            // Calculate the length of the captured pulse
            pulse_length1 += pow(2,(message1[palka] - 'a'));
        }
        else
        {
            printf("Nespravny vstup.\n");
            return 1;
        }
        palka++;
    }

    // Cteni zpravy prvni zpravy posle palky
    while (message1[palka + 1] != '\0')
    {
        if (message1[palka + 1] >= 'a' && message1[palka + 1] <= 'z' && message1[palka+1] != EOF)
        {
            // Calculate the length of the captured pulse
            message_length1 += pow(2,(message1[palka + 1] - 'a'));
        }
        else
        {
            printf("Nespravny vstup.\n");
            return 1;
        }
        palka++;
    }

    // Obnuleni palky aby se zacala cist druha zprava
    palka = 0;

    // Cteni pulzu druhe zpravy
    while (message2[palka] != '|')
    {
        if (message2[palka] >= 'a' && message2[palka] <= 'z' && message2[palka] != EOF)
        {
            // Calculate the length of the captured pulse
            pulse_length2 += pow(2,(message2[palka] - 'a'));
        }
        else
        {
            printf("Nespravny vstup.\n");
            return 1;
        }
        palka++;
    }

    // Cteni zpravy druhe zpravy posle palky
    while (message2[palka + 1] != '\0')
    {
        if (message2[palka + 1] >= 'a' && message2[palka + 1] <= 'z' && message2[palka+1] != EOF)
        {
            // Calculate the length of the captured pulse
            message_length2 += pow(2,(message2[palka + 1] - 'a'));
        }
        else
        {
            printf("Nespravny vstup.\n");
            return 1;
        }
        palka++;
    }

    // Scitani delky obou zprav
    int total_length1 = pulse_length1 + message_length1;
    int total_length2 = pulse_length2 + message_length2;

    // Scitani synchronizace

    //Synchronizace pokud obe zpravy jsou ve stejne casove delce
    if ((message_length1 == 0 && message_length2 == 0) || (pulse_length1 == 0 && pulse_length2 == 0) || (pulse_length1 == 0 && message_length2 == 0) || (message_length1 == 0 && pulse_length2 == 0))
    {
        printf("Synchronizace za: 0\n");
    }

    //Synchronizace pokud zpravy nejsou ve stejne casove delce
    else if (message_length1 != 0 && message_length2 != 0)
    {
        while (j <= 100000)
        {
            //Synchronizace pokud prvni pulze je vetsi nez druhe
            if (pulse_length1 > pulse_length2)
            {
                pulse_length2 += total_length2;
            }

            //Synchronizace pokud druhy pulze je vetsi nez prvni
            else if (pulse_length1 < pulse_length2)
            {
                pulse_length1 += total_length1;
            }

            // Syncronizace pokud j>100000
            else
            {
                break;
            }
            j++;

        }

        if (pulse_length1 == pulse_length2) {
            printf("Synchronizace za: %d\n", pulse_length1);
        }
        else
        {
            printf("Nelze dosahnout.\n");
        }
    }

    return 0;
}
